#!/bin/sh
ssh_config_file="$HOME/.ssh/config"

get_locations(){
	if ! grep -q "SSHFS Mount " "$ssh_config_file"
	then
		echo "<item label=\"Configure Locations...\">"
		echo "	<action name=\"Execute\">"
		echo "		<command>$HOME/.local/bin/sshfs_gui.sh</command>"
		echo "	</action>"
		echo "</item>"
	else
		while read -r line
		do
			location_path=""
			if echo "$line" | grep -q "SSHFS Mount "
			then
				location_path=$(echo "$line" | rev | cut -f 1 -d ' ' | rev)
				location_name=$(basename "$location_path")
				echo "<item label=\"$location_name\">"
				echo "	<action name=\"Execute\">"
				echo "		<command>thunar $location_path</command>"
				echo "	</action>"
				echo "</item>"
			fi
		done < "$ssh_config_file"
	fi
}

echo "<openbox_pipe_menu>"
echo "<item label=\"File Manager\">"
echo "	<action name=\"Execute\">"
echo "		<command>thunar</command>"
echo "	</action>"
echo "</item>"
echo "<separator label=\"Locations\" />"
		get_locations
echo "</openbox_pipe_menu>"
