#!/bin/sh

setup_script_path="$HOME/.local/bin/setup_gui.sh"

eval "$(xdotool getmouselocation --shell)"
mouse_x=$X
mouse_y=$Y

ssh_config_file="$HOME/.ssh/config"
server_name=$(grep -E "[H,h]ost " "$ssh_config_file" \
				| cut -f 2 -d ' ' \
				| tr '\n' '!' \
				| sed 's/!$//') # This only works in posix sh
sshfs_local_path="$HOME/Remote/myfolder"
sshfs_remote_path="$HOME"

get_field(){
    field_num="$1"
    info="$2"

    echo "$info" | cut -f "$field_num" -d '|'
}

error_box(){
    error_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --width 250 \
    --image "dialog-error" \
    --title "Error!" \
    --text "$error_text" \
    --button="OK":0
}

succ_box(){
    succ_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --width 250 \
    --image "dialog-info" \
    --title "Success!" \
    --text "$succ_text" \
    --button="OK":0
}

dec_box(){
    error_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --width 250 \
    --image "dialog-error" \
    --title "Error!" \
    --text "$error_text" \
    --button="No":1 \
    --button="Yes":0
    return $?
}

info_box(){
    info_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --no-buttons \
    --width 250 \
    --image "dialog-info" \
    --title "Info" \
    --text "$info_text"
}

get_info() {
	error_num=0
	error_msg=""

    info=$(yad \
                --posx="$mouse_x" \
                --posy="$mouse_y" \
				--width=400 \
                --title="New remote directory" \
                --text="Link to a remote directory" \
                --form \
                --field="SSH Config file":FL \
                --field="Server Shortname":CB \
                --field="Local Directory" \
                --field="Remote Directory" \
                --button="Configure Servers...":2 \
                --button="Cancel":1 \
                --button="Continue":0 \
                "$ssh_config_file" "$server_name" "$sshfs_local_path" "$sshfs_remote_path")	
	info_return="$?"
	if [ $info_return -eq 1 ] 
	then
		exit
	elif [ $info_return -eq 2 ]
	then
		if ! "$setup_script_path"
		then
			error_box "$setup_script_path did not finish successfully"
			get_info
		fi
	fi

	server_name=$(get_field 2 "$info")	
	sshfs_local_path=$(get_field 3 "$info")	
	sshfs_remote_path=$(get_field 4 "$info")	

    if test -z "$sshfs_local_path"
    then
            error_msg="No local path provided!"
            error_num=$(( error_num + 1))
    fi

    if test -z "$sshfs_remote_path"
    then
            error_msg="$error_msg\\nNo remote path provided"
            error_num=$(( error_num + 1))
    fi

    if [ $error_num -gt 0 ]
    then
        error_box "$error_msg" \
        && get_info
    fi

	configure_mount
}

configure_mount() {
    info_box "Attempting to link directories..." &
    info_pid=$(( $! + 2 )) # yikes, is this safe?

	test -e "$sshfs_local_path" \
	|| mkdir -p "$sshfs_local_path"
	
	if ! ssh "$server_name" "test -d $sshfs_remote_path"
	then
		if dec_box "Directory $sshfs_remote_path does not exist on remote host, create?"
		then
			if ! error_msg=$(ssh "$server_name" "mkdir $sshfs_remote_path" 2>&1)
			then
				error_box "Failed to create remote directory $error_msg"
			fi
		fi
	fi

	if error_msg=$(sshfs \
					"$server_name:$sshfs_remote_path" \
					"$sshfs_local_path" \
					-oauto_cache,reconnect,Compression=no 2>&1)
	then
		kill $info_pid
		succ_box "Successfuly linked $sshfs_local_path to $sshfs_remote_path on $server_name"
	else
		kill $info_pid
		error_box "Failed to link $sshfs_local_path to $sshfs_remote_path on $server_name\\n$error_msg"
	fi
}

get_info
