# tildemerge

Helps users connect and access files on tildes.
Part of TildeLinux.

### Tildes Supported
- tilde.town
- tilde.team
- yourtilde.com
- tilde.institute
- cosmic.voyage
 
### Dependencies
- gtk (gui)
- yad (gui)
- xdotool (gui)
- ssh
- sshfs
- ssh-askpass

### TODO
- Code cleanup
	- Equal signs before all yad options
	- Move shared functions into separate files
- Error handling for file creation
