#!/bin/sh

if test -z "$1"
then
	install_dir="$HOME/.local/bin"
else
	install_dir="$1"
fi

install_scripts(){
	for f in *.sh
	do
		if ! echo "$f" | grep -q "install.sh"
		then
			cp "$f" "$install_dir"
			echo "Copied $f to $install_dir/$f"
			chmod +x "$install_dir/$f"
			echo "Made $install_dir/$f executable"
			echo ""
		fi
	done
}

if test -f "$install_dir"
then
	install_scripts
else
	echo "$install_dir not found, creating..."
	mkdir -p "$install_dir"
	install_scripts
fi
