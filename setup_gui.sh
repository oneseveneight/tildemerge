#!/bin/sh
set -e

eval "$(xdotool getmouselocation --shell)"
mouse_x=$X
mouse_y=$Y

server_address="tilde.town!tilde.team!yourtilde.com!cosmic.voyage!tilde.institute"
server_name="MyTilde"
login=""
ssh_port="22"
ssh_dir="$HOME/.ssh"
priv_key_path=" "

get_field(){
    field_num="$1"
    info="$2"

    echo "$info" | cut -f "$field_num" -d '|'
}

error_box(){
    error_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --width 250 \
    --image "dialog-error" \
    --title "Error!" \
    --text "$error_text" \
    --button="OK":0
}

succ_box(){
    succ_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --width 250 \
    --image "dialog-info" \
    --title "Success!" \
    --text "$succ_text" \
    --button="OK":0
}

dec_box(){
    error_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --width 250 \
    --image "dialog-error" \
    --title "Error!" \
    --text "$error_text" \
    --button="No":1 \
    --button="Yes":0
    return $?
}

info_box(){
    info_text="$1"
    yad \
    --posx="$mouse_x" \
    --posy="$mouse_y" \
    --no-buttons \
    --width 250 \
    --image "dialog-info" \
    --title "Info" \
    --text "$info_text"
}

get_info() {
    error_num=0
    error_msg=""

    info=$(yad \
                --posx="$mouse_x" \
                --posy="$mouse_y" \
                --title="New server setup" \
                --text="Add a new server" \
                --form \
                --field="Server":CB \
                --field="Short Name" \
                --field="Login" \
                --field="Port" \
                --field="SSH Directory":DIR \
                --field="Private key":FL \
                --button="Cancel":1 \
                --button="Continue":0 \
                "$server_address" "$server_name" "$login" "$ssh_port" "$ssh_dir" "$priv_key_path")
    [ $? -eq 1 ] && exit 1

    server_address=$(get_field 1 "$info")
    server_name=$(get_field 2 "$info")
    login=$(get_field 3 "$info")
    ssh_port=$(get_field 4 "$info")
    ssh_dir=$(get_field 5 "$info")
    priv_key_path=$(get_field 6 "$info")
    
    if test -z "$login"
    then
            error_msg="No login provided!"
            error_num=$(( error_num + 1))
    fi

    if test -d "$(echo "$priv_key_path" | sed 's/\/ *$//')"
    then
        error_msg="$error_msg\\nNo private key provided!"
        error_num=$(( error_num + 1))
    fi

    if [ $error_num -gt 0 ]
    then
        error_box "$error_msg" \
        && get_info
    fi

    configure_server
}

configure_server() {
    ssh_current_config="$ssh_dir/config"
    ssh_temp_config="$ssh_dir/config.tmp"
    ssh_host_dir="$ssh_dir/$server_address"
    key_type=$(head -n 1 "$priv_key_path" \
                | cut -f 2 -d ' ' \
                | tr '[:upper:]' '[:lower:]')

    test -e "$ssh_current_config" \
    || touch "$ssh_current_config"
    cp "$ssh_current_config" "$ssh_temp_config"

    if grep -qi "Host $server_name" "$ssh_current_config"
    then
        dec_box "Host named $server_name already exists, overwrite?" \
        || get_info

        if ! dec_box "Keep old config in file? (ssh will use the new one)"
        then
            IFS=""
            in_old_host_block=0
            while read -r line
            do
                if echo "$line" | grep -q "Host $server_name"
                then
                    in_old_host_block=0
                elif echo "$line" | grep -q "Host "
                then
                    in_old_host_block=1
                fi
                
                [ $in_old_host_block -eq 1 ] \
                && echo "$line"
            done < "$ssh_current_config" > "$ssh_temp_config"
        fi
    fi

    mkdir -p "$ssh_host_dir"
    cp "$priv_key_path" "$ssh_host_dir/id_$key_type"

    {   
        echo "Host $server_name"
        echo "    HostName $server_address"
        echo "    Port $ssh_port"
        echo "    User $login"
        echo "    IdentityFile $ssh_host_dir/id_$key_type" 
    } >> "$ssh_temp_config"


    info_box "Attempting to log in with the provided credentials..." &
    info_pid=$(( $! + 2 )) # yikes, is this safe?
    echo "$ssh_temp_config" 
    echo "$server_address"
    if ssh -qF "$ssh_temp_config" "$server_name" exit
    then
        kill $info_pid
        succ_box "Login success!\\nAccount for $server_address created!"
        cp "$ssh_temp_config" "$ssh_current_config"
        rm "$ssh_temp_config"
        exit # Without this the "Host already exists" box appears, investigate.
    else
        kill $info_pid
        error_box "Login failed, please try again."
        get_info
    fi
}

get_info
