#!/bin/sh

# Copyright (C) 2018 oneseveneight
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
# 
# Email: oneseveneight@airmail.cc

is_substring(){
    case $2 in
    *$1*) 
        return 0 
        ;;
    *) 
        return 1 
        ;;
    esac
}

configure_directories() {
    server_name="$1"
    local_dir=""
    remote_dir=""
    
    echo "What would you like to call the local directory? (~/Remote/...)?"
    read -r answer
    local_dir="$HOME/Remote/$answer"
    mkdir -p "$local_dir"
    echo "Created directory $local_dir."
    echo "What directory on \"$server_name\" would you like to link it to? (absolute path)"
    read -r answer
    remote_dir="$answer"
    echo "Linking directory $local_dir to $remote_dir on \"$server_name\"."
    sshfs "$server_name:$remote_dir" "$local_dir"
    echo "Done, enjoy!"
}

attempt_login() {
    server_name="$1"
    ssh_temp_config="$2"
    ssh_config="$3"

    echo "Attempting to log in with configuration for \"$server_name\"."
    if ssh -qF "$ssh_temp_config" "$server_name" exit
    then
        echo "Login successful! Writing ssh config..."
        cp "$ssh_temp_config" "$ssh_config"
        echo "Would you like to configure sshfs for this server? (y/n)"
        read -r answer
        case "$answer" in
        "y")
            configure_directories "$server_name"
            ;;
        "n")
            echo "All right, you're all done!"
            echo "Exiting..."
            ;;
        "*")
            echo "\"$answer\" was not recognized as an answer, please enter 'y' or 'n'"
        esac
    else
        echo "Login failed, what would you like to do?"
        echo "    1) Try logging in again."
        echo "    2) Re-configure my credentials."
        read -r answer
        case "$answer" in
        "1")
            attempt_login "$server_name" "$ssh_temp_config" "$ssh_config"
            ;;
        "2")
            setup_keys "$server_address"
            ;;
        esac
    fi
}

setup_keys() {
    server_address="$1"
    server_name="$2"
    login=""
    key_path=""
    ssh_dir=""
    key_type=""

    echo "Configuring \"$server_address\" as \"$server_name\"."
    echo "What is your login name for this server?"
    read -r login
    echo "Where is the private key you use for this account (absolute path)"
    read -r key_path
    if test -r "$key_path"
    then
        echo "File found, configuring ssh..."
        ssh_dir="$HOME/.ssh"
        ssh_host_dir="$HOME/.ssh/$server_name"
        key_type=$(head -n 1 "$key_path" \
                    | cut -f 2 -d ' ' \
                    | tr '[:upper:]' '[:lower:]')
        test -d "$ssh_dir" || mkdir "$ssh_dir"
        test -d "$ssh_host_dir" || mkdir "$ssh_host_dir"
        echo "Key appears to be of type \"$key_type\""
        echo "Generating public key..."
        ssh-keygen -y -f "$key_path" > "$ssh_host_dir/id_$key_type.pub"
        echo "Copying private key..."
        cp "$key_path" "$ssh_host_dir/id_$key_type"
        echo "Saving configuration..."
        test -e "$ssh_dir/config" || touch "$ssh_dir/config"

        cp "$ssh_dir/config" "$ssh_dir/config.tmp"
        { 
            echo "Host $server_name"
            echo "    HostName $server_address"
            echo "    port 22"
            echo "    User $login"
            echo "    IdentityFile $ssh_host_dir/id_$key_type" 
        } >> "$ssh_dir/config.tmp"
        echo "Right now your configuration looks like this:"
        cat "$ssh_dir/config.tmp"

        attempt_login "$server_name" "$ssh_dir/config.tmp" "$ssh_dir/config"
    else
        echo "$key_path could not be read." 
        echo "Either it doesn't exist, or you dont have the correct permissions, please try again."
        setup_keys "$server_address" "$server_name"
    fi
}

pick_account() {
    answers="1 2 3 4"
    answer=""
    echo "What account would you like to configure?"
    echo "    1) tilde.town"
    echo "    2) tilde.team"
    echo "    3) yourtilde.com"
    echo "    4) cosmic.voyage"
    echo "    5) tilde.institute"
    read -r answer
    
    if is_substring "$answer" "$answers"
    then
        case "$answer" in
        "1")
            setup_keys "tilde.town" "town"
            ;;
        "2")
            setup_keys "tilde.team" "team"
            ;;
        "3")
            setup_keys "yourtilde.com" "your"
            ;;
        "4")
            setup_keys "cosmic.voyage" "cosmic"
            ;;
        "5")
            setup_keys "tilde.institute" "institute"
            ;;
        esac
    else
        echo "\"$answer\" is not a valid option."
        pick_account
    fi
}

pick_account
